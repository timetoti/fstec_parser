﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSTEC_Parser
{
    public partial class DetailsForm : Form
    {
        public DetailsForm(Threat threat)
        {
            InitializeComponent();
            this.Text = "Сведения об угрозе № " + threat.Id;
            dataGridView1.Rows.Add("Идентификатор УБИ", threat.Id);
            dataGridView1.Rows.Add("Наименование УБИ", threat.ThreatName);
            dataGridView1.Rows.Add("Описание", threat.ThreatDescription);
            dataGridView1.Rows.Add("Источник угрозы (характеристика и потенциал нарушителя)", threat.ThreatObject);
            dataGridView1.Rows.Add("Объект воздействия", threat.ThreatSource);
            dataGridView1.Rows.Add("Нарушение конфиденциальности", threat.Confidentiality ? "Присутствует" : "Отсутствует");
            dataGridView1.Rows.Add("Нарушение целостности", threat.Integrity ? "Присутствует" : "Отсутствует");
            dataGridView1.Rows.Add("Нарушение доступности", threat.Availability ? "Присутствует" : "Отсутствует");
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            MessageBox.Show(dataGridView1.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString());
        }
    }
}
