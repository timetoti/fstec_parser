﻿namespace FSTEC_Parser
{
    partial class WorkForm
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.обновитьБазуToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnLoadFromLocalFile = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnLoadFromSite = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnSaveInFile = new System.Windows.Forms.ToolStripMenuItem();
            this.BtnLastChanges = new System.Windows.Forms.ToolStripMenuItem();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.ThreatID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.ThreatName = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Details = new System.Windows.Forms.DataGridViewButtonColumn();
            this.BtnLeft = new System.Windows.Forms.Button();
            this.BtnRight = new System.Windows.Forms.Button();
            this.LabelPages = new System.Windows.Forms.Label();
            this.textBoxCurrentPage = new System.Windows.Forms.TextBox();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.обновитьБазуToolStripMenuItem,
            this.BtnSaveInFile,
            this.BtnLastChanges});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(789, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // обновитьБазуToolStripMenuItem
            // 
            this.обновитьБазуToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.BtnLoadFromLocalFile,
            this.BtnLoadFromSite});
            this.обновитьБазуToolStripMenuItem.Name = "обновитьБазуToolStripMenuItem";
            this.обновитьБазуToolStripMenuItem.Size = new System.Drawing.Size(100, 20);
            this.обновитьБазуToolStripMenuItem.Text = "Обновить базу";
            // 
            // BtnLoadFromLocalFile
            // 
            this.BtnLoadFromLocalFile.Name = "BtnLoadFromLocalFile";
            this.BtnLoadFromLocalFile.Size = new System.Drawing.Size(194, 22);
            this.BtnLoadFromLocalFile.Text = "Из локального файла";
            this.BtnLoadFromLocalFile.Click += new System.EventHandler(this.LoadFromLocalFile_Click);
            // 
            // BtnLoadFromSite
            // 
            this.BtnLoadFromSite.Name = "BtnLoadFromSite";
            this.BtnLoadFromSite.Size = new System.Drawing.Size(194, 22);
            this.BtnLoadFromSite.Text = "С сайта ФСТЭК";
            this.BtnLoadFromSite.Click += new System.EventHandler(this.LoadFromSite_Click);
            // 
            // BtnSaveInFile
            // 
            this.BtnSaveInFile.Name = "BtnSaveInFile";
            this.BtnSaveInFile.Size = new System.Drawing.Size(118, 20);
            this.BtnSaveInFile.Text = "Сохранить в файл";
            this.BtnSaveInFile.Visible = false;
            this.BtnSaveInFile.Click += new System.EventHandler(this.BtnSaveInFile_Click);
            // 
            // BtnLastChanges
            // 
            this.BtnLastChanges.Name = "BtnLastChanges";
            this.BtnLastChanges.Size = new System.Drawing.Size(143, 20);
            this.BtnLastChanges.Text = "Последние изменения";
            this.BtnLastChanges.Visible = false;
            this.BtnLastChanges.Click += new System.EventHandler(this.BtnLastChanges_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            this.dataGridView1.AllowUserToResizeColumns = false;
            this.dataGridView1.AllowUserToResizeRows = false;
            this.dataGridView1.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dataGridView1.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.ThreatID,
            this.ThreatName,
            this.Details});
            this.dataGridView1.Location = new System.Drawing.Point(45, 27);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            this.dataGridView1.RowHeadersVisible = false;
            this.dataGridView1.ScrollBars = System.Windows.Forms.ScrollBars.None;
            this.dataGridView1.Size = new System.Drawing.Size(700, 352);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // ThreatID
            // 
            this.ThreatID.HeaderText = "Идентификатор угрозы";
            this.ThreatID.Name = "ThreatID";
            this.ThreatID.ReadOnly = true;
            this.ThreatID.Width = 200;
            // 
            // ThreatName
            // 
            this.ThreatName.HeaderText = "Наименование угрозы";
            this.ThreatName.Name = "ThreatName";
            this.ThreatName.ReadOnly = true;
            this.ThreatName.Width = 400;
            // 
            // Details
            // 
            this.Details.FillWeight = 150F;
            this.Details.HeaderText = "";
            this.Details.Name = "Details";
            this.Details.ReadOnly = true;
            this.Details.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.Details.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            this.Details.Width = 97;
            // 
            // BtnLeft
            // 
            this.BtnLeft.Location = new System.Drawing.Point(10, 27);
            this.BtnLeft.Name = "BtnLeft";
            this.BtnLeft.Size = new System.Drawing.Size(25, 352);
            this.BtnLeft.TabIndex = 2;
            this.BtnLeft.Text = "<";
            this.BtnLeft.UseVisualStyleBackColor = true;
            this.BtnLeft.Visible = false;
            this.BtnLeft.Click += new System.EventHandler(this.BtnLeft_Click);
            // 
            // BtnRight
            // 
            this.BtnRight.Location = new System.Drawing.Point(755, 27);
            this.BtnRight.Name = "BtnRight";
            this.BtnRight.Size = new System.Drawing.Size(25, 352);
            this.BtnRight.TabIndex = 3;
            this.BtnRight.Text = ">";
            this.BtnRight.UseVisualStyleBackColor = true;
            this.BtnRight.Visible = false;
            this.BtnRight.Click += new System.EventHandler(this.BtnRight_Click);
            // 
            // LabelPages
            // 
            this.LabelPages.AutoSize = true;
            this.LabelPages.Location = new System.Drawing.Point(745, 384);
            this.LabelPages.Name = "LabelPages";
            this.LabelPages.Size = new System.Drawing.Size(0, 13);
            this.LabelPages.TabIndex = 4;
            this.LabelPages.Visible = false;
            // 
            // textBoxCurrentPage
            // 
            this.textBoxCurrentPage.Location = new System.Drawing.Point(705, 381);
            this.textBoxCurrentPage.Name = "textBoxCurrentPage";
            this.textBoxCurrentPage.Size = new System.Drawing.Size(40, 20);
            this.textBoxCurrentPage.TabIndex = 5;
            this.textBoxCurrentPage.Text = "1";
            this.textBoxCurrentPage.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
            this.textBoxCurrentPage.Visible = false;
            this.textBoxCurrentPage.TextChanged += new System.EventHandler(this.textBoxCurrentPage_TextChanged);
            // 
            // WorkForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(789, 406);
            this.Controls.Add(this.textBoxCurrentPage);
            this.Controls.Add(this.LabelPages);
            this.Controls.Add(this.BtnRight);
            this.Controls.Add(this.BtnLeft);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "WorkForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "База угроз";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem обновитьБазуToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem BtnLoadFromLocalFile;
        private System.Windows.Forms.ToolStripMenuItem BtnLoadFromSite;
        private System.Windows.Forms.ToolStripMenuItem BtnSaveInFile;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button BtnLeft;
        private System.Windows.Forms.Button BtnRight;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThreatID;
        private System.Windows.Forms.DataGridViewTextBoxColumn ThreatName;
        private System.Windows.Forms.DataGridViewButtonColumn Details;
        private System.Windows.Forms.ToolStripMenuItem BtnLastChanges;
        private System.Windows.Forms.Label LabelPages;
        private System.Windows.Forms.TextBox textBoxCurrentPage;
    }
}

