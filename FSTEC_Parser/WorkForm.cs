﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OfficeOpenXml; 
using System.IO;
using System.Net;

namespace FSTEC_Parser
{
    public partial class WorkForm : Form
    {
        const string nameadd = @"/thrlisl.xlsx";
        byte[] excelfile;
        List<Threat> threats = new List<Threat>();
        string lastchanges;
        int page = 1, maxpage;

        public WorkForm()
        {
            InitializeComponent();
        }

        private List<Threat> GetInf(string filename)
        {
            try
            {
                List<Threat> tthreats = new List<Threat>();
                using (ExcelPackage package = new ExcelPackage(new FileInfo(filename)))
                {
                    var sheet = package.Workbook.Worksheets.First();
                    var totalRows = sheet.Dimension.End.Row;
                    var totalColumns = sheet.Dimension.End.Column;

                    for (int rowNum = 3; rowNum <= totalRows; rowNum++)
                    {
                        var row = sheet.Cells[rowNum, 1, rowNum, totalColumns].Select(c => c.Value == null ? string.Empty : c.Value.ToString());
                        string[] temp = string.Join("|", row).Split('|');
                        tthreats.Add(new Threat(Int32.Parse(temp[0]), temp[1], temp[2], temp[3], temp[4], temp[5] == "1" ? true : false, temp[6] == "1" ? true : false, temp[7] == "1" ? true : false));
                    }
                    excelfile = File.ReadAllBytes(filename);
                }
                return tthreats;
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
                return null;
            }
        }

        private void UpdateGrid()
        {
            dataGridView1.Rows.Clear();

            for (int i = (page-1)*15; (i < threats.Count) && (i < (page)*15); i++)
            {
                dataGridView1.Rows.Add(threats[i].Id, threats[i].ThreatName, "Подробнее");
            }
        }

        private bool CheckUpdates(List<Threat> tthreats)
        {
            if (tthreats!=null)
            {
                lastchanges = "";
                int count = 0;
                foreach (Threat t in tthreats)
                {
                    if (t.Id <= threats.Count)
                    {
                        if (!threats[t.Id-1].Equals(t))
                        {
                            count++;
                            lastchanges += threats[t.Id - 1].ToString() + '\n' + t.ToString() + '\n';
                        }
                    }
                    else
                    {
                        count++;
                    }
                }
                lastchanges = count.ToString() + '\n' + lastchanges;
                threats = tthreats;
                maxpage = threats.Count / 15 + (threats.Count % 15 != 0 ? 1 : 0);
                LabelPages.Text = "/ " + maxpage;
                return true;
            }
            else
            {
                return false;
            }
        }

        private void SaveFile(string folderpath)
        {
            File.WriteAllBytes(folderpath + nameadd, excelfile);
            MessageBox.Show("Успешно!");
        }

        private void LoadFromLocalFile_Click(object sender, EventArgs e)
        {
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                if (CheckUpdates(GetInf(ofd.FileName)))
                {
                    UpdateGrid();
                    if (BtnLastChanges.Visible == false)
                    {
                        BtnLastChanges.Visible = true;
                        BtnSaveInFile.Visible = true;
                        BtnLeft.Visible = true;
                        BtnRight.Visible = true;
                        LabelPages.Visible = true;
                        textBoxCurrentPage.Visible = true;
                    }
                    MessageBox.Show("Уcпешно!");
                }
                else
                {
                    MessageBox.Show("Ошибка считывания базы");
                }
            }
        }

        private void LoadFromSite_Click(object sender, EventArgs e)
        {
                string tempFileName = @"D:\thrlist.xlsx";
            try
            {
                new WebClient().DownloadFile(@"https://bdu.fstec.ru/documents/files/thrlist.xlsx", tempFileName);
                if (CheckUpdates(GetInf(tempFileName)))
                {
                    UpdateGrid();
                    if(BtnLastChanges.Visible == false)
                    {
                        BtnLastChanges.Visible = true;
                        BtnSaveInFile.Visible = true;
                        BtnLeft.Visible = true;
                        BtnRight.Visible = true;
                        LabelPages.Visible = true;
                        textBoxCurrentPage.Visible = true;
                    }
                    MessageBox.Show("Уcпешно!");
                }
                else
                {
                    MessageBox.Show("Ошибка считывания базы");
                }
            }
            catch(Exception ex)
            {
                MessageBox.Show("Ошибка\n" + ex.Message);
            }
            finally
            {
                if (File.Exists(tempFileName))
                {
                    File.Delete(tempFileName);
                }
            }
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex==2)
            {
                new DetailsForm(threats[e.RowIndex + 1]).ShowDialog();
            }
        }

        private void BtnSaveInFile_Click(object sender, EventArgs e)
        {
            FolderBrowserDialog fbd = new FolderBrowserDialog();
            fbd.Description = "Выберите папку для сохранения базы угроз";
            if (fbd.ShowDialog()==DialogResult.OK)
            {
                SaveFile(fbd.SelectedPath);
            }
        }

        private void BtnLastChanges_Click(object sender, EventArgs e)
        {
            new LastChangesForm(lastchanges).ShowDialog();
        }

        private void BtnLeft_Click(object sender, EventArgs e)
        {
            if (page>1)
            {
                textBoxCurrentPage.Text = (page - 1).ToString();
            }
        }

        private void BtnRight_Click(object sender, EventArgs e)
        {
            if (page<maxpage)
            {
                textBoxCurrentPage.Text = (page + 1).ToString();
            }
        }

        private void textBoxCurrentPage_TextChanged(object sender, EventArgs e)
        {
            try
            {
                int p = Int32.Parse(textBoxCurrentPage.Text);
                if (1 <= p && p <= maxpage)
                {
                    page = p;
                    UpdateGrid();
                    textBoxCurrentPage.BackColor = Color.White;
                }
                else
                {
                    throw new Exception();
                }
            }
            catch(Exception)
            {
                textBoxCurrentPage.BackColor = Color.IndianRed;
            }
        }
    }
}
