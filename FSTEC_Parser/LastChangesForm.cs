﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FSTEC_Parser
{
    public partial class LastChangesForm : Form
    {
        public LastChangesForm(string lastchanges)
        {
            InitializeComponent();
            string[] changes = lastchanges.Split('\n');
            int all = Int32.Parse(changes[0]);
            int edited = changes.Length / 2 - 1;
            string text = "Количество новых угроз: " + (all - edited) + '\n' + "Количество измененных угроз: " + edited + "\n\n";

            if (edited > 0)
            {
                listBox1.Text += '\n';
                for (int i = 1; i < edited * 2; i += 2)
                {
                    string[] t0 = changes[i].Split('|');
                    string[] t1 = changes[i + 1].Split('|');

                    text += "Идентификатор измененной угрозы: " + t0[0] + '\n';

                    string[] properties = { "Наименование УБИ", "Описание", "Источник угрозы (характеристика и потенциал нарушителя)", "Объект воздействия", "Нарушение конфиденциальности", "Нарушение целостности", "Нарушение доступности" };

                    for (int j=1; j<8; j++)
                    {
                        if (!t0[j].Equals(t1[j]))
                        {
                            text += properties[j - 1] + "\nБыло: " + t0[j] + "\nСтало: " + t1[j] + '\n';
                        }
                    }
                    text += '\n';
                }
            }

            listBox1.DataSource = text.Split('\n'); ;
        }
    }
}
